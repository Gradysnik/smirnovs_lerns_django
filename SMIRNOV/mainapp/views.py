from django.shortcuts import render


def main(request):
    return render(request, 'mainapp/index.html')


def about(request):
    return render(request, 'mainapp/about.html')


def work(request):
    return render(request, 'mainapp/work.html')


def contact(request):
    return render(request, 'mainapp/contact.html')

